from django.shortcuts import render, redirect
from django.contrib import messages
from .models import User
from .models import iteams

# Create your views here.
def index(request):
	# iteams.objects.all().delete()

	return render(request, 'BeltExam_App/index.html')

def wishlist(request):
	context = {
		# 'Users' : User.objects.all(),
		'iteams' : iteams.objects.all(),
	}
	return render(request, 'BeltExam_App/wishlist.html', context)


def processREG(request):
	if User.objects.isValidFirst(request.POST['first_name']) and User.objects.isValidLast(request.POST['last_name']) and User.objects.isValidPassword(request.POST['password']):
		messages.error(request, 'Please Fill out the ALL the Boxs.')
		return redirect ('/')
	else:
		if User.objects.isValidEmail(request.POST['email']):
			if request.POST['password'] != request.POST['Cpassword']:
				messages.error(request, 'Your password and Confirmation password do  not match.')
				return redirect ('/')
			else:
				User.objects.create(email=request.POST['email'], first_name=request.POST['first_name'], last_name=request.POST['last_name'], password=request.POST['password'])
				request.session['name'] = request.POST['first_name']
				print "******ALL USERS******"
				users = User.objects.all()
				for u in users:
					print "email:",u.email, "password:",u.password
			return redirect ('/wishlist')
		else:
			messages.error(request, 'The Email Address You Entered:'+request.POST['email']+' is not a valid Email Address.')
			return redirect ('/')

def processLOG(request):
	if User.objects.filter(email = request.POST['email'], password = request.POST['password']):
		THEUSER = User.objects.filter(email=request.POST['email'])[0]
		request.session['name'] = THEUSER.first_name
		# print THEUSER.first_name
		return redirect ('/wishlist')
	else:
		messages.warning(request, 'Password or Email is incorrect.')

		# User.objects.all().delete()
		print "Email you tried:",request.POST['email']
		print "Password you tried:",request.POST['password']
		users = User.objects.all()
		print "******ALL USERS******"
		for u in users:
			print "email:",u.email, "password:",u.password

		return redirect('/')

def ADDiteam(request):
	if iteams.objects.isValid(request.POST['iteam']):
		messages.warning(request, 'No Iteam to add')
		return redirect('/wishlist')
	else:
		iteams.objects.create(iteam=request.POST['iteam'])
		messages.info(request, 'Iteam Added')
		iteam = iteams.objects.all()
		for i in iteam:
			print "iteam Added:", i.iteam
		return redirect('/wishlist')

def logout(request):
	request.session['name'] =''
	return redirect('/') 