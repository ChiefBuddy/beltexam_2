from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.index),
	url(r'^wishlist$', views.wishlist),
	url(r'^processREG$', views.processREG),
	url(r'^processLOG$', views.processLOG),
	url(r'^ADDiteam$', views.ADDiteam),
	url(r'^logout$', views.logout)

	]