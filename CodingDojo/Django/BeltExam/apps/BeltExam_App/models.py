from __future__ import unicode_literals

from django.db import models
import re

# Create your models here.
EMAIL = re.compile (r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')
EMPTY = re.compile (r'^..?$')

class UserManager(models.Manager):
	def isValidEmail(self, email):
		if EMAIL.match(email):
			return True
		else:
			return False

	def isValidFirst(self, first_name):
		if EMPTY.match(first_name):
			return True
		else:
			return False

	def isValidLast(self, last_name):
		if EMPTY.match(last_name):
			return True
		else:
			return False

	def isValidPassword(self, password):
		if EMPTY.match(password):
			return True
		else:
			return False


class User(models.Model):
	first_name = models.CharField(max_length=20)
	last_name = models.CharField(max_length=20)
	email = models.EmailField()
	password = models.CharField(max_length=50)
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True)
	# iteams = models.ForeignKey(iteams)
	objects = UserManager()

class IteamsManager(models.Manager):
	def isValid(self, iteam):
		if EMPTY.match(iteam):
			return True
		else:
			return False

class iteams(models.Model):
	iteam = models.CharField(max_length=20)
	# name_User = models.ForeignKey(User)
	# name = models.ForeignKey(User, on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True)
	objects = IteamsManager()
