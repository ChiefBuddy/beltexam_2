'Local Settings'@
Music/
'My Documents'@
NetHood@
NTUSER.DAT
ntuser.dat.LOG1
ntuser.dat.LOG2
NTUSER.DAT{3fbdca8c-6898-11e6-bf9a-ad9c737fc5b3}.TM.blf
NTUSER.DAT{3fbdca8c-6898-11e6-bf9a-ad9c737fc5b3}.TMContainer00000000000000000001.regtrans-ms
NTUSER.DAT{3fbdca8c-6898-11e6-bf9a-ad9c737fc5b3}.TMContainer00000000000000000002.regtrans-ms
ntuser.ini
OneDrive/
Pictures/
PrintHood@
Recent@
Roaming/
'Saved Games'/
Searches/
SendTo@
'Start Menu'@
Templates@
Tracing/
Videos/

Travis@CB MINGW64 ~
$ clear

Travis@CB MINGW64 ~
$ pwd
/c/Users/Travis

Travis@CB MINGW64 ~
$ cd desktop

Travis@CB MINGW64 ~/desktop (master)
$ pwd
/c/Users/Travis/desktop

Travis@CB MINGW64 ~/desktop (master)
$ mkdir branching_merging

Travis@CB MINGW64 ~/desktop (master)
$ cd branching_merging

Travis@CB MINGW64 ~/desktop/branching_merging (master)
$ pwd
/c/Users/Travis/desktop/branching_merging

Travis@CB MINGW64 ~/desktop/branching_merging (master)
$ touch index.html

Travis@CB MINGW64 ~/desktop/branching_merging (master)
$ ls
index.html

Travis@CB MINGW64 ~/desktop/branching_merging (master)
$ subl index.html

Travis@CB MINGW64 ~/desktop/branching_merging (master)
$ ls
index.html

Travis@CB MINGW64 ~/desktop/branching_merging (master)
$ git init
Initialized empty Git repository in C:/Users/Travis/Desktop/branching_merging/.git/

Travis@CB MINGW64 ~/desktop/branching_merging (master)
$ git add .

Travis@CB MINGW64 ~/desktop/branching_merging (master)
$ git status
On branch master

Initial commit

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

        new file:   index.html


Travis@CB MINGW64 ~/desktop/branching_merging (master)
$ git commit -m 'start of html'
[master (root-commit) 8e53608] start of html
 1 file changed, 26 insertions(+)
 create mode 100644 index.html

Travis@CB MINGW64 ~/desktop/branching_merging (master)
$ git branch add-css

Travis@CB MINGW64 ~/desktop/branching_merging (master)
$ git checkout add-css
Switched to branch 'add-css'

Travis@CB MINGW64 ~/desktop/branching_merging (add-css)
$ ls
index.html

Travis@CB MINGW64 ~/desktop/branching_merging (add-css)
$ subl index.html

Travis@CB MINGW64 ~/desktop/branching_merging (add-css)
$ checkout master
bash: checkout: command not found

Travis@CB MINGW64 ~/desktop/branching_merging (add-css)
$ git checkout master
M       index.html
Switched to branch 'master'

Travis@CB MINGW64 ~/desktop/branching_merging (master)
$ subl index.html

Travis@CB MINGW64 ~/desktop/branching_merging (master)
$ git checkout add-css
M       index.html
Switched to branch 'add-css'

Travis@CB MINGW64 ~/desktop/branching_merging (add-css)
$ ls
index.html

Travis@CB MINGW64 ~/desktop/branching_merging (add-css)
$ git add .

Travis@CB MINGW64 ~/desktop/branching_merging (add-css)
$ git status
On branch add-css
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        modified:   index.html


Travis@CB MINGW64 ~/desktop/branching_merging (add-css)
$ git commit -m 'added css'
[add-css 3e38e78] added css
 1 file changed, 117 insertions(+)

Travis@CB MINGW64 ~/desktop/branching_merging (add-css)
$ checkout master
bash: checkout: command not found

Travis@CB MINGW64 ~/desktop/branching_merging (add-css)
$ git checkout master
Switched to branch 'master'

Travis@CB MINGW64 ~/desktop/branching_merging (master)
$ pwd
/c/Users/Travis/desktop/branching_merging

Travis@CB MINGW64 ~/desktop/branching_merging (master)
$ cd ..

Travis@CB MINGW64 ~/desktop (master)
$ pwd
/c/Users/Travis/desktop

Travis@CB MINGW64 ~/desktop (master)
$
